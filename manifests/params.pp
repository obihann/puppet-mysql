class mysql::params {
  $package_ensure  = "present"
  $server_name     = "mysql-server"
  $client_name     = "mysql-client"
  $service_name    = "mysql"
  $service_ensure  = "running"
  $root_name       = "root"
  $root_password   = "toor"
  $path            = "/usr/bin/"
}
