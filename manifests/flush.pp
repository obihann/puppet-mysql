class mysql::flush (
  $server_name    = $mysql::params::server_name,
  $client_name    = $mysql::params::client_name,
  $root_name      = $mysql::params::root_name,
  $root_password  = $mysql::params::root_password,
  $path           = $mysql::params::path,
) inherits mysql::params {
  exec { "mysql -u$root_name -p$root_password -e 'flush privileges'":
    path => $path,
    require => Package[$server_name, $client_name],
  }
}
