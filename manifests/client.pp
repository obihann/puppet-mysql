class mysql::client (
  $package_ensure = $mysql::params::package_ensure,
  $client_name    = $mysql::params::client_name,
) inherits mysql::params {
  package { $client_name:
    ensure => $package_ensure,
  }
}
