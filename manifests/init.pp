class mysql (
  $package_ensure  = $mysql::params::package_ensure,
  $server_name     = $mysql::params::server_name,
  $client_name     = $mysql::params::client_name,
  $service_name    = $mysql::params::service_name,
  $service_ensure  = $mysql::params::service_ensure,
  $root_name       = $mysql::params::root_name,
  $root_password   = $mysql::params::root_password,
  $path            = $mysql::params::params,
) inherits mysql::params {
  class { "::mysql::client":
    package_ensure => $package_ensure,
    client_name    => $client_name,
  }

  class { "::mysql::server":
    package_ensure => $package_ensure,
    server_name    => $server_name,
    service_name   => $service_name,
  }

  class { "::mysql::service":
    service_ensure => $service_ensure,
    server_name    => $server_name,
    service_name   => $service_name,
  }
}
