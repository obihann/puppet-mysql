class mysql::service (
  $service_ensure = $mysql::params::service_ensure,
  $server_name    = $mysql::params::server_name,
  $service_name   = $mysql::params::service_name,
) inherits mysql::params {
  service { $service_name:
    ensure => $service_ensure,
    require => Package[$server_name],
  }
}
