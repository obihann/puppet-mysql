class mysql::server (
  $package_ensure = $mysql::params::package_ensure,
  $server_name    = $mysql::params::server_name,
  $client_name    = $mysql::params::client_name,
  $service_name   = $mysql::params::service_name,
) inherits mysql::params {
  package { $server_name:
    ensure => $package_ensure,
    notify => Service[$service_name],
  }
}
