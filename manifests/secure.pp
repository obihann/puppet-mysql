class mysql::secure (
  $server_name    = $mysql::params::server_name,
  $client_name    = $mysql::params::client_name,
  $root_name      = $mysql::params::root_name,
  $root_password  = $mysql::params::root_password,
  $path           = $mysql::params::path,
) inherits mysql {
      exec { "mysqladmin -u $root_name password $root_password":
          path => $path,
          require => Package[$server_name, $client_name],
      }
}
