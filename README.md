# Puppet MySQL

This is a basic puppet module that installs mysql.

## Basic Usage
For a default mysql-server and mysql-client installation.
```
include mysql
```

## Secure Setup
For installation of mysql-server and mysql-client as well
changing the password of `root` to `toor`.

```
include mysql::secure
```

### Secure Setup With Custom Password
For installation of mysql-server and mysql-client, with your
choice of a password for `root`.

```
class { "mysql::secure":
    root_password => "newpassword",
}
```

## Flush Database Privileges

```
include mysql::flush
```
